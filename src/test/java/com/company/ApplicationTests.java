package com.company;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.company.repo.companyRepo.CompanyRepo;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(properties = "spring.datasource.type=com.zaxxer.hikari.HikariDataSource")
public class ApplicationTests {

	@Autowired
	@Qualifier("CompanyOracleDataSource")
	private DataSource dataSource;
	
	@Autowired
	private CompanyRepo companyRepo;

	@Test
	public void hikariConnectionPoolIsConfigured() {
		assertEquals("com.zaxxer.hikari.HikariDataSource", dataSource.getClass().getName());
	}
	
	@Test
	public void testConnection() {
		assertNotNull(companyRepo.findAll(PageRequest.of(1, 10)));
	}


}
