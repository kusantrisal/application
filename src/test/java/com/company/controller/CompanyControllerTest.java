package com.company.controller;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.company.domain.Company;
import com.company.repo.companyRepo.CompanyRepo;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CompanyControllerTest {

	@Mock
	private CompanyRepo companyRepo;
	
	@InjectMocks
	private CompanyController companyController;
	
	private MockMvc mockMvc;

	@Before
	public void setup() throws Exception {
		mockMvc = MockMvcBuilders.standaloneSetup(companyController).build();
	
	}

	//@formatter:off
	
	@Test
	public void testFindById() throws Exception {
	Company	c = new Company(1, "Centene", "12-3456789", "ACTIVE", "12/31/2019", null, null,null);
		when(companyRepo.findById(anyLong())).thenReturn(Optional.of(c));
		mockMvc.perform(get("/main/company/findCompanyById/1"))
			   .andExpect(status().isOk())
			   .andExpect(jsonPath("$.companyId",Matchers.is(1)))
			   .andExpect(jsonPath("$.name",Matchers.is("Centene")))
			   .andExpect(jsonPath("$.tin",Matchers.is("12-3456789")))
			   .andExpect(jsonPath("$.status",Matchers.is("ACTIVE")))
			   .andExpect(jsonPath("$.establishedDate",Matchers.is("12/31/2019")))
			   .andExpect(jsonPath("$.*",Matchers.hasSize(8)));
	}
	
	//@formatter:on

}
