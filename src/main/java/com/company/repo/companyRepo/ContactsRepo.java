package com.company.repo.companyRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.company.domain.Contact;

@Repository
public interface ContactsRepo extends JpaRepository<Contact,Long> {

}
