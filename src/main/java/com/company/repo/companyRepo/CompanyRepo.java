package com.company.repo.companyRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.company.domain.Company;

@Repository
public interface CompanyRepo extends JpaRepository<Company,Long> {
	
	@Query(value = "select * from COMPANY.COM_INFO c where c.COM_ID =:id", nativeQuery =  true)
	Company findByIdCustom(@Param("id") long id);
	
	@Query(value = "select * from company.COM_INFO c where c.NAME =:name and rownum < 2", nativeQuery =  true)
	Company findByName(String name);

	@Modifying(clearAutomatically = true)
	@Query(value = "UPDATE Company.COM_INFO SET status = :status WHERE com_id = :id and tin = :tin",nativeQuery = true)
	int updateCompanyStatus(String id, String tin, String status);

}
