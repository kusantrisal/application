package com.company.repo.companyRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.company.domain.BusinessType;

@Repository
public interface BusinessTypeRepo extends JpaRepository<BusinessType,Long> {

}
