package com.company.repo.companyRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.company.domain.Location;

@Repository
public interface LocationRepo extends JpaRepository<Location,Long> {


}
