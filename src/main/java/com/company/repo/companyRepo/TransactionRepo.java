package com.company.repo.companyRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.company.domain.Transaction;

@Repository
public interface TransactionRepo extends JpaRepository<Transaction, Long> {

}
