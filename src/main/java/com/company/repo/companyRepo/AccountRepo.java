package com.company.repo.companyRepo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.company.domain.Account;

@Repository
public interface AccountRepo extends JpaRepository<Account, Long> {

}
