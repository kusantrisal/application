package com.company.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.utils.Variables;
import com.company.domain.Account;
import com.company.domain.Transaction;
import com.company.repo.companyRepo.AccountRepo;
import com.company.repo.companyRepo.BusinessTypeRepo;
import com.company.repo.companyRepo.CompanyRepo;
import com.company.repo.companyRepo.ContactsRepo;
import com.company.repo.companyRepo.LocationRepo;
import com.company.repo.companyRepo.TransactionRepo;

import io.swagger.annotations.Api;

@RestController
@PropertySource(value = "classpath:queries.xml")
@Api(value = "AccountsInfo", description = "Accounts Controller")
@RequestMapping("/main/company/account")
public class AccountController {

	public static final Logger logger = LogManager.getLogger(AccountController.class);

	@Autowired
	private CompanyRepo companyRepo;

	@Autowired
	private LocationRepo locationRepo;

	@Autowired
	private ContactsRepo contactRepo;

	@Autowired
	private BusinessTypeRepo businessTypeRepo;

	@Autowired
	private AccountRepo accountRepo;

	@Autowired
	private TransactionRepo transactionRepo;

	@GetMapping("/createAccount/withCompanyId/{id}")
	public Account createAccount(@PathVariable("id") long id) {
		Account acct = new Account(Long.valueOf(0), companyRepo.findById(id).get(), "PAID", "500.00",
				Variables.currentTimeString, null,null);
		return accountRepo.save(acct);
	}

	@GetMapping("/createTransaction/withAccountId/{id}")
	public Transaction createTransactionWithAccountId(@PathVariable("id") long id) {
		Transaction trans = new Transaction(Long.valueOf(0), Variables.currentTimeString, "", "", "", "",
				accountRepo.findById(id).get(), null);
		return transactionRepo.save(trans);
	}

}
