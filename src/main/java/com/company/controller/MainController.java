package com.company.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.swagger.annotations.Api;

@Lazy
@RestController
@PropertySource(value = "classpath:queries.xml")
@Api(value = "Company", description = "Description info")
@RequestMapping("/main")
public class MainController {

	public static final Logger logger = LogManager.getLogger(MainController.class);

	@Autowired
	private EntityManager em;

	@GetMapping("/junit/test")
	public String junit() {
		return "junit-test";
	}

	@GetMapping("/hostIp")
	@Transactional
	public String ip(Authentication auth) {

		// Gson gson = new GsonBuilder().setPrettyPrinting().create();
		// String json = gson.toJson(overview);
		// return json;

		String sql = "select ora_database_name from dual";
		Query query = em.createNativeQuery(sql);
		String dbName = (String) query.getResultList().get(0);

		InetAddress IP = null;
		try {
			IP = InetAddress.getLocalHost();
			logger.info("Application host IP address " + IP.getHostAddress());
		} catch (UnknownHostException e) {
			logger.error("Error encounted", e);
		}
		return auth.getName() +" "+ auth.isAuthenticated()+" "+auth.getDetails() ;
	}
	
//URL sample http://localhost:9000/main/dir/C:_Users_kushw_git_application_logs
	
	@GetMapping("/dir/{path}")
	public List<String> dir(@PathVariable("path") String path) {

		try {
			return Files.walk(Paths.get(path.replaceAll("_", "/")), 1)
					// .filter(Files::isRegularFile)
					// .filter(Files::isDirectory)
					// .filter(s -> !s.toString().endsWith(".ini"))
					// .filter(s -> s.toString().startsWith("."))
					.map(p -> p.toString().replace("\\", "_")).collect(Collectors.toList());

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("/readme/{path}")
	public String logs(@PathVariable("path") String path) {
		try {
			return Files.lines(Paths.get(path.replaceAll("_", "/"))).collect(Collectors.joining("\n"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "Error";
	}
}
