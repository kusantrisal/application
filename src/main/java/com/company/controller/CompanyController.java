package com.company.controller;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.company.domain.BusinessType;
import com.company.domain.Company;
import com.company.domain.Contact;
import com.company.domain.Location;
import com.company.repo.companyRepo.BusinessTypeRepo;
import com.company.repo.companyRepo.CompanyRepo;
import com.company.repo.companyRepo.ContactsRepo;
import com.company.repo.companyRepo.LocationRepo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * Company Controller holds the controller to fetch and write the Company,
 * location, business type, contacts entity
 * 
 * @author Kushwant
 *
 */
@RestController
@PropertySource(value = "classpath:queries.xml")
@Api(value = "CompanyInfo", description = "Company Controller info")
@RequestMapping("/main/company")
public class CompanyController {

	public static final Logger logger = LogManager.getLogger(CompanyController.class);

	@Autowired
	private CompanyRepo companyRepo;

	@Autowired
	private LocationRepo locationRepo;

	@Autowired
	private ContactsRepo contactRepo;

	@Autowired
	private BusinessTypeRepo businessTypeRepo;

	// Company

	@GetMapping("/debug")
	public List<Company> debug() {
		return companyRepo.findAll();
	}
	
	@GetMapping("/findAllCompanies/{page}")
	public List<Company> findAllCompanies(@PathVariable("page") int page) {
		return companyRepo.findAll(PageRequest.of(page, 5)).getContent();
	}
	
	@GetMapping("/findAllCompaniesMv/{page}")
	public ModelAndView findAllCompaniesMv(@PathVariable("page") int page, ModelAndView mv) {
		
		List<Company> companies = companyRepo.findAll(PageRequest.of(page, 5)).getContent();
		mv.setViewName("company");
		
		Field[] fields = Company.class.getDeclaredFields();
		List<String> privateFieldList = Arrays.asList(fields).stream().filter(field -> Modifier.isPrivate(field.getModifiers()))
				.map(f -> f.getName())
				.filter(h -> !h.contains("locations"))
				.filter(h -> !h.contains("account"))
				.collect( Collectors.toList());
		
		mv.addObject("companyHeader", privateFieldList );
		mv.addObject("companies", companies );
		return mv;
	}

	@GetMapping("/findCompanyById/{id}")
	public Optional<Company> findCompanyById(@PathVariable("id") long id) {
		return companyRepo.findById(id);
	}

	@PostMapping("/addCompany")
	public String addCompany(@Valid @ModelAttribute("company") Company company, BindingResult result) {
		return result.hasErrors() ? result.toString() : companyRepo.save(company).toString();
	}

	/**
	 * Company ID and TIN number must be passed to update the status Only takes
	 * ACTIVE or DEACTIVED as status.
	 * 
	 * @param request
	 * @return {@link Integer} 1 = True and 0 = False
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(name = "ID", value = "Company Id", required = true, dataType = "string", paramType = "query"),
			@ApiImplicitParam(name = "TIN", value = "12-3456789", required = true, dataType = "string", paramType = "query"),
			@ApiImplicitParam(name = "STATUS", value = "ACTIVE or DEACTIVATED", required = true, dataType = "string", paramType = "query") })
	@PostMapping("/updateCompanyStatus")
	@Transactional
	@ApiOperation(value = "Updates the Company Status", notes = "Allowed values ACTIVE and DEACTIVED", response = Integer.class, responseContainer = "Interger")
	public int updateCompany(HttpServletRequest request) {
		return companyRepo.updateCompanyStatus(request.getParameter("id"), request.getParameter("tin"),
				request.getParameter("status"));
	}

	// Location

	@GetMapping("/findAllLocations/{page}")
	public Page<Location> findAllLocations(@PathVariable("page") int page) {
		return locationRepo.findAll(PageRequest.of(page, 10));
	}

	@GetMapping("/findLocationById/{id}")
	public Optional<Location> findLocationById(@PathVariable("id") long id) {
		return locationRepo.findById(id);
	}

	// Contact

	@GetMapping("/findAllContacts/{page}")
	public Page<Contact> findAllContacts(@PathVariable("page") int page) {
		return contactRepo.findAll(PageRequest.of(page, 10));
	}

	@GetMapping("/findContactById/{id}")
	public Optional<Contact> findContactById(@PathVariable("id") long id) {
		return contactRepo.findById(id);
	}

	// Business Type

	@GetMapping("/findAllBusinessType/{page}")
	public Page<BusinessType> findAllBusinessType(@PathVariable("page") int page) {
		return businessTypeRepo.findAll(PageRequest.of(page, 10));
	}

	@GetMapping("/findBusinessTypeById/{id}")
	public Optional<BusinessType> findBusinessTypeById(@PathVariable("id") long id) {
		return businessTypeRepo.findById(id);
	}

}
