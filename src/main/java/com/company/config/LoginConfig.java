package com.company.config;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginConfig {

	@GetMapping("/login")
	public String home() {
		return "login";
	}

	@RequestMapping("/home")
	public ModelAndView handleError(ModelAndView mv) {
		mv.addObject("greetings", "Welcome ");
		mv.setViewName("index");
		return mv;
	}

}
