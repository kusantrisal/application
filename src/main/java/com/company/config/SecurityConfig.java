package com.company.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http.csrf().disable()
					.authorizeRequests()
					.antMatchers("/home","/resources/**").permitAll()
					.antMatchers("/admin/**").hasRole("ADMIN")
					.antMatchers("/anonymous*").anonymous()
					.antMatchers("/login*").permitAll()
					.anyRequest().authenticated()
					.and().formLogin().loginPage("/login")
						  .loginProcessingUrl("/perform_login")
						  .defaultSuccessUrl("/home", true)
				 .failureUrl("/login.html?error=true")
				// .failureHandler(authenticationFailureHandler())
				.and().logout().logoutUrl("/perform_logout").deleteCookies("JSESSIONID");
		// .logoutSuccessHandler(logoutSuccessHandler());
	}

}
