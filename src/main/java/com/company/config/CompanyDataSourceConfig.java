package com.company.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import com.company.domain.BusinessType;
import com.company.domain.Company;
import com.company.domain.Location;
import com.company.repo.companyRepo.BusinessTypeRepo;
import com.company.repo.companyRepo.CompanyRepo;
import com.company.repo.companyRepo.LocationRepo;

@Configuration
@EnableJpaRepositories(basePackageClasses = { CompanyRepo.class, LocationRepo.class,
		BusinessTypeRepo.class }, entityManagerFactoryRef = "CompanyOracleEntityManager", transactionManagerRef = "CompanyOracleTransanctionManager")
@EnableAutoConfiguration
public class CompanyDataSourceConfig {

	@Bean(name = "CompanyOracleDataSourceProp")
	@ConfigurationProperties(prefix = "company.oracle.datasource")
	@Primary
	public DataSourceProperties companyOracleDataSourceProp() {
		return new DataSourceProperties();
	}

	@Bean(name = "CompanyOracleDataSource")
	@Primary
	public DataSource companyOracleDataSource(
			@Qualifier("CompanyOracleDataSourceProp") DataSourceProperties oracleDataSourceProp) {
		return oracleDataSourceProp.initializeDataSourceBuilder().build();
	}

	@Bean(name = "CompanyOracleEntityManager")
	@Primary
	public LocalContainerEntityManagerFactoryBean CompanyOracleEntityManagerFactoryBean(
			@Qualifier("CompanyOracleDataSource") DataSource dataSource, EntityManagerFactoryBuilder builder) {
		LocalContainerEntityManagerFactoryBean oracleLocalEntityManagerFactoryBean = builder.dataSource(dataSource)
				.packages(Company.class, Location.class, BusinessType.class).build();

		Properties props = new Properties();
		props.put("hibernate.hbm2ddl.auto", "create-drop");
		props.put("hibernate.show_sql", "true");
	    props.put("hibernate.dialect", "org.hibernate.dialect.Oracle12cDialect");
		props.put("hibernate.default_schema", "COMPANY");
		oracleLocalEntityManagerFactoryBean.setPersistenceUnitName("Company_Api_Oracle_Datasource");
		oracleLocalEntityManagerFactoryBean.setJpaProperties(props);
		oracleLocalEntityManagerFactoryBean.afterPropertiesSet();
	//	oracleLocalEntityManagerFactoryBean.setPackagesToScan("com.company.domain.Company");
		return builder.dataSource(dataSource).packages(Company.class,Location.class,BusinessType.class).build();
	}

	@Bean(name = "CompanyOracleTransanctionManager")
	@Primary
	public PlatformTransactionManager companyOracleTransanctionManager(
			@Qualifier("CompanyOracleEntityManager") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

	@Bean(name = "CompanyJdbcTemplate")
	@Primary
	public NamedParameterJdbcTemplate companyJdbcTemplate(@Qualifier("CompanyOracleDataSource") DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}
}
