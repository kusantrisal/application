package com.company.components;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.company.utils.Variables;
import com.company.domain.BusinessType;
import com.company.domain.Company;
import com.company.domain.Contact;
import com.company.domain.Location;
import com.company.repo.companyRepo.AccountRepo;
import com.company.repo.companyRepo.BusinessTypeRepo;
import com.company.repo.companyRepo.CompanyRepo;
import com.company.repo.companyRepo.ContactsRepo;
import com.company.repo.companyRepo.LocationRepo;

@Service
@Profile("dev")
public class CustomApplicationListener implements ApplicationListener<ApplicationReadyEvent> {

	public static final Logger logger = LogManager.getLogger(CustomApplicationListener.class);

	@Autowired
	private CompanyRepo companyRepo;

	@Autowired
	private LocationRepo locationRepo;

	@Autowired
	private ContactsRepo contactRepo;

	@Autowired
	private BusinessTypeRepo businessTypeRepo;

	@Autowired
	private AccountRepo accountRepo;

	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		logger.info("Application Started.");
	}

	@Override
	@Transactional
	public void onApplicationEvent(ApplicationReadyEvent event) {

		logger.info("Loading Data in the Database for development and testing");

		for (int i = 0; i < 10; i++) {
			BusinessType p = new BusinessType(0, null, "Whatever",null);
			Company company = new Company(0, "Centene", "12-3456789", "ACTIVE", Variables.currentTimeString,null,
					businessTypeRepo.save(p),null);
			Location location = new Location(0, companyRepo.save(company), Variables.currentTimeString, "123 Main St", "",
					"St Louis", "MO", "75069", "USA", null,null);
			Contact contact = new Contact(0, "123-456-7891", "123-456-7891", "Email@mail.com",
					locationRepo.save(location),null);
			contactRepo.save(contact);
		}
		

		logger.info("Data inserted");

	}

}
