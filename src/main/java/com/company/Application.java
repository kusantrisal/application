package com.company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Company Application
 * @author kushwant
 * 
 * NOTE:Item and the User domain, repo and datasource are for development and testing purpose only.
 *
 */
@SpringBootApplication
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Configuration
	@Profile("test")
	@ComponentScan(lazyInit = true)
	static class LocalConfig {

	}

}
