package com.company.utils;

public interface Constants {

	String Constant = "Constant";
	
	String ERROR_MSG_RECORD_STATUS = "Record Status must be in [P|D|T]-MM/DD/YYYY format.(Primary,Deactivated or Terminated " ;
	String REGEX_RECORD_STATUS = "[T|D|P]-\\d{2}/\\d{2}/\\d{4}";
	
	String ERROR_MSG_DATE_FORMAT = "Date Must be in MM/dd/yyyy format.";
	String REGEX_DATE_FORMAT = "[\\d]{1,2}/[\\d]{1,2}/[\\d]{4}";
	
	
	
}
