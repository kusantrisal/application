package com.company.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Variables {
	
	public static String currentTimeString = LocalDate.now().format(DateTimeFormatter.ofPattern("MM/dd/yyyy"));

}
