package com.company.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;

import com.company.utils.Constants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "COM_INFO")
@Data
public class Company {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "COM_ID")
	private long companyId;

	private String name;

	@Pattern(regexp = "\\d{1,2}-\\d{7}", message = "Tin must be in XX-XXXXXX format")
	private String tin;

	@Pattern(regexp = "ACTIVE|DEACTIVATED", message = "Value must be either ACTIVE or INACTIVE.")
	private String status;

	@Column(name = "ESTABLISHED_DATE")
	@Pattern(regexp = Constants.REGEX_DATE_FORMAT, message = Constants.ERROR_MSG_DATE_FORMAT)
	private String establishedDate;
	
	//best way to map one to many
	@Transient
	@Column(name = "FK_COMPANY_ID", insertable = false, updatable = false)
	private List<Location> locations;

	
	@ManyToOne(targetEntity = BusinessType.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "FK_BUSINESS_TYPE_ID")
	private BusinessType businessType;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "company",  orphanRemoval = true)
	@Transient
	private Account account;

}
