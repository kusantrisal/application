package com.company.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import com.company.utils.Constants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "COM_ACCOUNT")
@Data
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "ACCT_ID")
	private Long accountId;

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "FK_COMPANY_ID")
    private Company company;

    @Column(name = "ACCOUNT_STATUS")
	private String accountStatus;
	
	private String amount;
	
	@Pattern(regexp = Constants.REGEX_DATE_FORMAT, message = Constants.ERROR_MSG_DATE_FORMAT)
	@Column(name = "UPDATED_DATE")
	private String updatedDate;
	
	@OneToMany(mappedBy = "accountTran", fetch = FetchType.LAZY)
	private List<Transaction> transactions = new ArrayList<Transaction>();
	
	@Column(name="RECORD_STATUS")
	@Pattern(regexp = Constants.REGEX_RECORD_STATUS, message = Constants.ERROR_MSG_RECORD_STATUS)
	private String recordStatus;

}
