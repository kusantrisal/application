package com.company.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import com.company.utils.Constants;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "COM_CONTACTS")
public class Contact {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "CONTACT_ID")
	private long contactId;
	
	private String phone;
	
	private String fax;
	
	private String email;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_LOCATION_ID")
	@JsonIgnore
	private Location locContact;
	
	@Column(name="RECORD_STATUS")
	@Pattern(regexp = Constants.REGEX_RECORD_STATUS, message = Constants.ERROR_MSG_RECORD_STATUS)
	private String recordStatus;
	
	

}
