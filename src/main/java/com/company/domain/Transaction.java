package com.company.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import com.company.utils.Constants;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "COM_TRANSACTION")
public class Transaction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "TRANSACTION_ID")
	private Long transactionId;
	
	@Pattern(regexp = Constants.REGEX_DATE_FORMAT, message = Constants.ERROR_MSG_DATE_FORMAT)
	@Column(name = "UPDATED_DATE")
	private String updatedDate;
	
	@Column(name = "UPDATED_FROM")
	private String updatedFrom;
	
	@Column(name = "UPDATED_BY")
	private String updatedBy;
	
	@Column(name = "TRANSACTION_TYPE")
	private String transactionType;
	
	@Column(name = "TRANSACTION_SUMMARY")
	private String transactionSummary;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ACCOUNT_ID")
	@JsonIgnore
	private Account accountTran;
	
	@Column(name="RECORD_STATUS")
	@Pattern(regexp = Constants.REGEX_RECORD_STATUS, message = Constants.ERROR_MSG_RECORD_STATUS)
	private String recordStatus;

}
