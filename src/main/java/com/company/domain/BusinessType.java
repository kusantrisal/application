package com.company.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;

import com.company.utils.Constants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "COM_BUSINESS_TYPE")
public class BusinessType {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long businessTypeId;

	@Transient
	@Column(name = "FK_BUSINESS_TYPE_ID", insertable = false, updatable = false)
	private List<Company> companies = new ArrayList<Company>();

	private String description;

	@Column(name="RECORD_STATUS")
	@Pattern(regexp = Constants.REGEX_RECORD_STATUS, message = Constants.ERROR_MSG_RECORD_STATUS)
	private String recordStatus;
}
