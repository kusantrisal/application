package com.company.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import com.company.utils.Constants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "COM_LOCATION")
public class Location {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "LOCATION_ID")
	private long locationId;

	@ManyToOne(targetEntity = Company.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_COMPANY_ID")
	private Company companyloc;

	@Column(name = "LOCATION_ESTABLISHED_DATE")
	@Pattern(regexp = Constants.REGEX_DATE_FORMAT, message = Constants.ERROR_MSG_DATE_FORMAT)
	private String establishedDate;

	@Column(name = "STREET_1")
	private String street1;

	@Column(name = "STREET_2")
	private String street2;

	private String city;

	private String state;

	private String zip;

	private String country;

	@OneToMany(mappedBy = "locContact")
	private List<Contact> contacts = new ArrayList<Contact>();
	
	@Column(name="RECORD_STATUS")
	@Pattern(regexp = Constants.REGEX_RECORD_STATUS, message = Constants.ERROR_MSG_RECORD_STATUS)
	private String recordStatus;

}
