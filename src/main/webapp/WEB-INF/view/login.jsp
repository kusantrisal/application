<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Login</title>
</head>

<body>
	<header>
		<jsp:include page="header.jsp" />
	</header>

	<div class="container">

		<form class="form-horizontal" action="/perform_login" method="post" style = "margin-top:25px;">

			<div class="form-group">

				<div class="alert alert-danger" id="alert"
						style="visibility: hidden; margin-top: 25px; width:82%;">
					<strong>Danger!</strong> You have selected the Production server.
				</div>
				<label class="control-label col-sm-2" for="server">Server:</label>
				<div class="col-sm-10">
					<select class="form-control" id="selectBox" name="server"
						onchange="changeFunc();" required>
						<option value="Dev">Dev</option>
						<option value="Test">Test</option>
						<option value="Prod">Prod</option>
					</select>
				</div>
			</div> 
			<script type="text/javascript">
				function changeFunc() {

					var selectBox = document.getElementById("selectBox");
					var selectedValue = selectBox.options[selectBox.selectedIndex].value;
					if (selectedValue == "Prod") {
						document.getElementById("alert").style.visibility = "visible";
					}
				}
			</script>

			<div class="form-group">
				<label class="control-label col-sm-2" for="username">Username:</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="username"
						placeholder="Enter Username" name="username" required>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="password">Password:</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" id="password"
						placeholder="Enter password" name="password" required>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary">Login</button>
				</div>
			</div>
		</form>

	</div>


	<footer>
		<jsp:include page="footer.jsp" />
	</footer>
</body>

</html>
