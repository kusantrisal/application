<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
<!-- <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1"> -->
<!-- <link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">	 -->

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand/logo -->
		<a class="navbar-brand" href="/home">Company</a>

		<!-- Links -->
		<ul class="navbar-nav ">
			<li class="nav-item "><a class="nav-link" href="/main/company/findAllCompaniesMv/1">Company</a></li>
			<li class="nav-item "><a class="nav-link" href="/swagger"  target="_blank">Swagger</a></li>
			<li class="nav-item"><a class="nav-link" href="#">Link3</a></li>
		</ul>

		<ul class="navbar-nav ml-auto">

			<c:choose>
				<c:when test="${not empty pageContext.request.userPrincipal}">
					<li class="nav-item"><a class="nav-link" href="/home"> <security:authorize
								access="isAuthenticated()">
								<security:authentication property="principal" />
							</security:authorize>
					</a></li>
					<li class="nav-item "><a class="nav-link"
						href="/perform_logout">Logout</a></li>
				</c:when>
				<c:otherwise>
					<li class="nav-item"><a class="nav-link" href="/login">Login</a>
					</li>
				</c:otherwise>
			</c:choose>

		</ul>
	</nav>

</body>
</html>