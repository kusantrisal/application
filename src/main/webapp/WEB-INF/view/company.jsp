<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<title>Home</title>
</head>

<body>
	<header>
		<jsp:include page="header.jsp" />
	</header>






	<div class="container" style="margin-top: 25px;">
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<c:forEach items="${companyHeader}" var="comh">
							<th>${comh}</th>
						</c:forEach>
				</thead>
				<tbody>
					<c:forEach items="${companies}" var="com">
						<tr>
						<td>${com.companyId}</td>
							<td>${com.name}</td>
							<td>${com.companyId}</td>
							<td>${com.tin}</td>
							<td>${com.establishedDate}</td>
							<td>${com.businessType.description}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>

	</div>







	</div>
	<footer>
		<jsp:include page="footer.jsp" />
	</footer>
</body>

</html>
