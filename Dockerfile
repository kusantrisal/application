FROM openjdk:8
ADD target/company-api-1.0.0-SNAPSHOT-spring-boot.war company-api-1.0.0-SNAPSHOT.war
EXPOSE 9000
ENTRYPOINT ["java", "-jar","company-api-1.0.0-SNAPSHOT.war" ]